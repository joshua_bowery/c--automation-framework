﻿using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;
using System.Drawing;
using UI.Logging;

// TODO: Wrap findelement method to return Element class

namespace UI.Selenium
{
    /// <summary>
    /// Extended class of IWebElement
    /// </summary>
    public class Element : IWebElement
    {
        private readonly IWebElement _element;

        public readonly string Name;

        public By FoundBy { get; set; }

        public Element(IWebElement element, string name)
        {
            _element = element;
            Name = name;
        }


        public string TagName => _element.TagName;

        public string Text => _element.Text;

        public bool Enabled => _element.Enabled;

        public bool Selected => _element.Selected;

        public Point Location => _element.Location;

        public Size Size => _element.Size;

        public bool Displayed => _element.Displayed;

        public IWebElement Current => _element ?? throw new NullReferenceException("_elemnt is null.");

        /// <summary>
        /// Clears out a text field
        /// </summary>
        public void Clear()
        {
            LogHelper.Log?.Info("Clearing out field");
            _element.Clear();
        }

        /// <summary>
        /// Clicks on an IWebElement
        /// </summary>
        public void Click()
        {
            LogHelper.Log?.Step($"Clicking on {Name}");
            Driver.ScrollToView(Current);
            _element.Click();
        }

        /// <summary>
        /// Return a child element of an element
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public IWebElement FindElement(By by)
        {
            LogHelper.Log?.Info($"Finding element at {by} under {Name}");
            return _element.FindElement(by);
        }

        /// <summary>
        /// return a list of child elements
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return _element.FindElements(by);
        }

        /// <summary>
        /// returns the given attribute value
        /// </summary>
        /// <param name="attributeName"></param>
        /// <returns></returns>
        public string GetAttribute(string attributeName)
        {
            return _element.GetAttribute(attributeName);
        }

        /// <summary>
        /// Returns the css vaue of a property
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public string GetCssValue(string propertyName)
        {
            return _element.GetCssValue(propertyName);
        }

        /// <summary>
        /// Returns the property
        /// </summary>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public string GetProperty(string propertyName)
        {
            return _element.GetProperty(propertyName);
        }

        /// <summary>
        /// Send Keys to an IWebElement
        /// </summary>
        /// <param name="text"></param>
        public void SendKeys(string text)
        {
            LogHelper.Log?.Step($"Sending {text} to {Name}");
            _element.SendKeys(text);
        }

        /// <summary>
        /// Submits a form
        /// </summary>
        public void Submit()
        {
            LogHelper.Log?.Step("Submitting fields");
            _element.Submit();
        }
    }
}

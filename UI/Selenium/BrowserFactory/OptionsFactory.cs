﻿using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;

namespace UI.Selenium.BrowserFactory
{
    public static class OptionsFactory
    {
        public static ChromeOptions GetChromeOptions(bool headless, bool incognito)
        {
            ChromeOptions options = new ChromeOptions();

            if (headless == true) { options.AddArguments("--headless"); }
            if (incognito == true) { options.AddArguments("--incognito"); }
            return options;
        }

        public static FirefoxOptions GetFirefoxOptions(bool headless, bool incognito)
        {
            FirefoxOptions options = new FirefoxOptions();

            if (headless == true) { options.AddArguments("--headless"); }
            if (incognito == true) { options.AddArguments("--incognito"); }
            return options;
        }
    }
}

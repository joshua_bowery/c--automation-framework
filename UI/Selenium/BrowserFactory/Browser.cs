﻿namespace UI.Selenium.BrowserFactory
{
    public enum Browser
    {
        Firefox,
        Chrome,
        InternetExplorer,
        Edge,
        Safari
    }
}

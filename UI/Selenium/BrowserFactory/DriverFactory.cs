﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;

namespace UI.Selenium.BrowserFactory
{
    public static class DriverFactory
    {

        public static IWebDriver BuildLocal(Browser browser, bool headless, bool incognito)
        {
            switch (browser)
            {
                case Browser.Chrome:
                    return new ChromeDriver(OptionsFactory.GetChromeOptions(headless, incognito));
                case Browser.Firefox:
                    return new FirefoxDriver(OptionsFactory.GetFirefoxOptions(headless, incognito));
                default:
                    return new ChromeDriver();
            }


        }

        public static IWebDriver BuildRemote(Browser browser, string url, bool headless = false, bool incognito = false, int seconds = 600)
        {
            switch (browser)
            {
                case Browser.Chrome:
                    return RemoteChrome(url, headless, incognito, seconds);
                default:
                    return new ChromeDriver();
            }
        }

        private static IWebDriver RemoteChrome(string url, bool headless = false, bool incognito = false, int seconds = 600)
        {
            var options = OptionsFactory.GetChromeOptions(headless, incognito);
            return new RemoteWebDriver(
                new Uri(url), options.ToCapabilities(), TimeSpan.FromSeconds(seconds));
        }


    }
}

﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;

// TODO: fix class to reflect Element class changes

namespace UI.Selenium
{
    public class ActionsClass
    {

        private Actions _actions;
        private IWebDriver _driver;

        public ActionsClass(IWebDriver driver)
        {
            _driver = driver;
            _actions = new Actions(Driver.Current);
        }

        public ActionsClass MoveToElement(Element element)
        {
            _actions.MoveToElement(element.Current);
            return this;

        }

        public void Perform()
        {
            _actions.Perform();
        }


    }
}

﻿using OpenQA.Selenium;
using System;
using System.Collections.ObjectModel;
using UI.Logging;
using UI.Selenium.BrowserFactory;
using UI.Waiting;

namespace UI.Selenium
{
    /// <summary>
    /// Wrapper class to work with selenium driver. Must initialize Driver with Initialize method
    /// </summary>
    public static class Driver
    {

        [ThreadStatic]
        private static IWebDriver _driver;

        public static IWebDriver Current => _driver ?? throw new NullReferenceException("_driver is null.");

        [ThreadStatic]
        public static Wait Wait;


        /// <summary>
        /// Current Url Value
        /// </summary>
        public static string Url => _driver.Url;

        /// <summary>
        /// Title of webpage
        /// </summary>
        public static string Title => _driver.Title;


        public static string PageSource => _driver.PageSource;


        public static string CurrentWindowHandle => _driver.CurrentWindowHandle;


        public static ReadOnlyCollection<string> WindowHandles => _driver.WindowHandles;


        public static void InitializeLocal(Browser browser, bool headless = false, bool incognito = false)
        {
            LogHelper.Log?.Info($"Starting up a local {browser} Driver");

            _driver = DriverFactory.BuildLocal(browser, headless, incognito);
            Wait = new Wait(15);
        }

        public static void InitializeRemote(Browser browser, string url, bool headless = false, bool incognito = false, int seconds = 600)
        {
            throw new NotImplementedException();
            LogHelper.Log.Info($"Starting up a Remote {browser} Driver");
            //_driver = DriverFactory.BuildRemote(Browser.Chrome,"gg");
            Wait = new Wait(15);
        }

        /// <summary>
        /// Returns an object of the Element Class.
        /// </summary>
        /// <param name="by"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static Element FindElement(By by, string name)
        {
            LogHelper.Log?.Info($"Looking for Element {name} at {by}");

            var element = Wait.Until(drv => drv.FindElement(by));
            return new Element(element, name)
            {
                FoundBy = by
            };
        }

        /// <summary>
        /// Closes current tab
        /// </summary>
        public static void Close()
        {
            LogHelper.Log?.Info("Closing current tab");

            _driver.Close();
        }

        /// <summary>
        /// Quits driver session
        /// </summary>
        public static void Quit()
        {
            LogHelper.Log?.Info("Closing current driver session");
            _driver.Quit();
        }

        /// <summary>
        /// Returns an IOptions object
        /// </summary>
        /// <returns></returns>
        public static IOptions Manage()
        {
            return _driver.Manage();
        }

        /// <summary>
        /// returns an INavigation object
        /// </summary>
        /// <returns></returns>
        public static INavigation Navigate()
        {
            return _driver.Navigate();
        }

        /// <summary>
        /// returns an ITargetLocator object
        /// </summary>
        /// <returns></returns>
        public static ITargetLocator SwitchTo()
        {
            return _driver.SwitchTo();
        }

        /// <summary>
        /// Returns a collection of elements
        /// </summary>
        /// <param name="by"></param>
        /// <returns></returns>
        public static Elements FindElements(By by)
        {
            return new Elements(_driver.FindElements(by))
            {
                FoundBy = by
            };
        }


        public static void Dispose()
        {
            _driver.Dispose();
        }

        /// <summary>
        /// Go to a webpage
        /// </summary>
        /// <param name="url"></param>
        public static void GoTo(string url)
        {
            LogHelper.Log?.Info($"Vavigating to {url}");
            _driver.Navigate().GoToUrl(url);
        }

        public static void ExecuteJavaScript(string script)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)_driver;
            js.ExecuteScript(script);
        }

        public static void ExecuteJavaScript(string script, IWebElement element)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)_driver;
            js.ExecuteScript(script, element);
        }

        public static string ExecuteReturnJavaScript(string script)
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)_driver;
            return js.ExecuteScript(script).ToString();

        }

        public static void GoBack()
        {
            Driver.Navigate().Back();
        }

        public static void GoForward()
        {
            Driver.Navigate().Forward();
        }

        public static void Refresh()
        {
            Driver.Navigate().Refresh();
        }

        public static void SwitchToTab(string tabTitle)
        {
            var windows = Driver.WindowHandles;

            foreach (string window in windows)
            {
                if (tabTitle.Equals(Driver.Title))
                {
                    break;
                }
            }
        }

        public static void ScrollTo(int xPosition = 0, int yPosition = 0)
        {
            var js = string.Format("window.scrollTo({0}, {1})", xPosition, yPosition);
            ExecuteJavaScript(js);
        }

        public static void ScrollToView(IWebElement element)
        {
            if (element.Location.Y > 200)
            {
                ScrollTo(0, element.Location.Y - 100); // Make sure element is in the view but below the top navigation pane
            }

        }


    }
}

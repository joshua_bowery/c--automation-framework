﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using System;
using System.Threading;
using UI.Selenium;
using UI.Selenium.BrowserFactory;

namespace Framework.UnitTests.Selenium
{
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class DriverTests
    {

        [OneTimeSetUp]
        public void SetUpTests()
        {

        }

        [Test]
        [TestCase(Browser.Chrome, "ChromeDriver")]
        [TestCase(Browser.Firefox, "FirefoxDriver")]
        public void InitilizeLocalTest(Browser browser, string name)
        {
            Driver.InitializeLocal(browser);
            var x = Driver.Current.GetType();



            Assert.Multiple(() =>
            {
                Assert.AreEqual(name, x.Name);
                Assert.IsNotNull(Driver.Wait);
            });
        }

        
        [Test]
        [TestCaseSource("GetUserLists")]
        public void FindElementCanProduceAnElementClassObjectForEachByTest(By by)
        {
            try
            {
                Driver.InitializeLocal(Browser.Chrome);
                Driver.GoTo("https://the-internet.herokuapp.com/");
            }
            catch(Exception x)
            {
                Assert.Inconclusive();
            }
            
            Element element = Driver.FindElement(by, "");

            try
            {
                Assert.NotNull(element);

            }
            catch (Exception x)
            {

                Assert.Fail();
            }
            finally
            {
                Driver.Quit();
            }
        }



        [Test]
        public void CloseMethodClosesTab()
        {
            
            try
            {
                Driver.InitializeLocal(Browser.Chrome);
                Driver.GoTo("https://the-internet.herokuapp.com/");
                Element link = Driver.FindElement(By.XPath("//li[1]//a[1]"),"");
                Actions action = new Actions(Driver.Current);
                action.KeyDown(Keys.Control).MoveToElement(link.Current).Click().Perform();
            }
            catch (Exception x)
            {
                Assert.Inconclusive();
            }

            var tabCount1 = Driver.WindowHandles.Count;
            Driver.Close();
            var tabCount2 = Driver.WindowHandles.Count;

            try
            {
                Assert.AreEqual(tabCount1-1 , tabCount2);
            }
            catch (Exception x)
            {
                
                Assert.Fail();
            }
            finally
            {
                Driver.Quit();
            }





        }

        [Test]
        public void QuitMethodClosesBrowserSession()
        {
            try
            {
                Driver.InitializeLocal(Browser.Chrome);
                Driver.GoTo("https://the-internet.herokuapp.com/");
                Element link = Driver.FindElement(By.XPath("//li[1]//a[1]"), "");
                Actions action = new Actions(Driver.Current);
                action.KeyDown(Keys.Control).MoveToElement(link.Current).Click().Perform();
            }
            catch (Exception x)
            {
                Assert.Inconclusive();
            }

            Driver.Quit();
            Assert.That(() => Driver.Current.Url,
                Throws.Exception
                .TypeOf<OpenQA.Selenium.WebDriverException>());
        }

        [Test]
        [Ignore("Need to test running process closed. Will implement later")]
        public void DisposeMethodDisposesOfDriver()
        {
            try
            {
                Driver.InitializeLocal(Browser.Chrome);
                Driver.GoTo("https://the-internet.herokuapp.com/");
                Element link = Driver.FindElement(By.XPath("//li[1]//a[1]"), "");
                Actions action = new Actions(Driver.Current);
                action.KeyDown(Keys.Control).MoveToElement(link.Current).Click().Perform();
            }
            catch (Exception x)
            {
                Assert.Inconclusive();
            }

            
            
            Driver.Quit();
            Driver.Dispose();
            Assert.IsNull(Driver.Current);
        }

        

        private static object[] GetUserLists =
        {
            new object[]{ By.XPath("//li[1]//a[1]")},
            new object[]{ By.Id("content") },
            new object[]{ By.CssSelector("div.row:nth-child(2) div.large-12.columns:nth-child(2) ul:nth-child(4) li:nth-child(1) > a:nth-child(1)") },
            new object[]{ By.LinkText("A/B Testing") },
            new object[]{ By.TagName("a") }




        };
    }
}

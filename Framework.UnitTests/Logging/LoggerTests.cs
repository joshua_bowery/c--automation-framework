using NUnit.Framework;
using NUnit.Framework.Internal;
using System.IO;

namespace Framework.UnitTests.Logging
{
    //ToDo: Create tests for log methods
    [TestFixture]
    [Parallelizable(ParallelScope.All)]
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [TestCase("existingString", "C:\\Users\\joshu\\OneDrive\\Desktop\\UnitTestResults\\test1.txt")]
        public void LogFileCreated(string testName, string filePath)
        {
            UI.Logging.Logger testLogger = new UI.Logging.Logger(testName, filePath);

            string text = File.ReadAllText(filePath);

            Assert.Multiple(() =>
            {
                Assert.IsTrue(File.Exists(filePath));
                Assert.IsNotNull(text);
            });
        }

        [Test]
        [TestCase("existingString", null)]
        public void LogFileCreated2(string testName, string? filePath)
        {
            UI.Logging.Logger testLogger = new UI.Logging.Logger(testName, filePath);

            Assert.IsTrue(!File.Exists(filePath));
        }


    }
}